import en from './en/en';
import fr from './fr/fr';

export const resources = {
  en: {
    translation: {
      ...en,
    },
  },
  fr: {
    translation: {
      ...fr,
    },
  },
};
