import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  btn_container: {
    borderRadius: 3,
    height: 42,
    width: 'auto',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btn_text: {
    fontSize: 16,
    margin: 8,
  },
  primary_container: {
    backgroundColor: '#007bff',
  },
  primary_text: {
    color: 'white',
  },
  secondary_container: {
    borderWidth: 1,
    borderColor: '#007bff',
    backgroundColor: 'white',
  },
  secondary_text: {
    color: '#007bff',
  },
});
