import React from 'react';
import {View, Text, Image} from 'react-native';
import i18n from '../../i18n';
import {logoGT} from '../../assets';
import styles from './styles';

export default ({style}) => (
  <View style={[styles.designByContainer, {...style}]}>
    <Text style={styles.designByText}>{i18n.t('designed_by')}</Text>
    <View style={styles.designByImgContainer}>
      <Image source={logoGT} style={styles.designByImg} />
    </View>
  </View>
);
