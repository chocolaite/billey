import React from 'react';
import {Icon} from 'react-native-elements';
import {mainStyles} from '../../assets';
import {View} from 'react-native';

export default ({onPress}) => (
  <View style={mainStyles.btnMenuLeft}>
    <Icon
      name="bars"
      type={'font-awesome-5'}
      size={35}
      onPress={() => onPress()}
    />
  </View>
);
