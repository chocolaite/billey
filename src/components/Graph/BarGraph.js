import React from 'react';
import {VictoryBar, VictoryChart, VictoryTheme} from 'victory-native';
import {ActivityIndicator} from 'react-native';
import styles from './styles';

export const BarGraph = ({data, isLoading = false}) => {
  if (isLoading) {
    return <ActivityIndicator size="large" style={styles.loadingContainer} />;
  }
  return (
    <VictoryChart theme={VictoryTheme.material} domainPadding={{x: 15}}>
      <VictoryBar
        data={data}
        labels={({datum}) => datum.y}
        style={{
          data: {
            strokeWidth: 0,
            fill: ({datum}) => datum.color,
          },
        }}
      />
    </VictoryChart>
  );
};
