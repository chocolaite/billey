import i18n from 'i18next';
import {initReactI18next} from 'react-i18next';
import {resources} from './lang';
import {NativeModules, Platform} from 'react-native';

function getLocalLang() {
  const deviceLanguage =
    Platform.OS === 'ios'
      ? NativeModules.SettingsManager.settings.AppleLocale ||
        NativeModules.SettingsManager.settings.AppleLanguages[0] //iOS 13
      : NativeModules.I18nManager.localeIdentifier;
  return deviceLanguage.toString().substring(0, 2);
}

i18n.use(initReactI18next).init({
  resources,
  lng: getLocalLang(),
  keySeparator: false, // we do not use keys in form messages.welcome
  interpolation: {
    escapeValue: false, // react already safes from xss
  },
  react: {
    useSuspense: false,
  },
});

export default i18n;
