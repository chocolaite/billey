import React from 'react';
import {withTranslation} from 'react-i18next';
import {Text, View} from 'react-native';
import {mainStyles} from '../../../assets';
import {addCategoryBudget, getAccounts} from '../actions';
import {connect} from 'react-redux';
import {CATEGORIES_BUDGET} from '../actions/types';
import {CategoryForm} from '../components/Form';

class ForecastNewCategoryBudgetView extends React.Component {
  state = {
    currency: '',
  };
  async componentDidMount() {
    const accounts = await getAccounts();
    const currency = accounts[0].currency;
    // eslint-disable-next-line react/no-did-mount-set-state
    this.setState({currency});
  }

  handleOnSave = (category) => {
    const {dispatch, navigation} = this.props;
    dispatch(addCategoryBudget(category, CATEGORIES_BUDGET)).then(() => {
      navigation.goBack();
    });
  };
  render() {
    const {t, navigation} = this.props;
    const {currency} = this.state;
    return (
      <View style={mainStyles.container}>
        <CategoryForm
          name={t('new')}
          amount={'0'}
          currency={currency}
          keyText={'new'}
          onSave={this.handleOnSave}>
          <Text
            style={[mainStyles.textAlignCenter, mainStyles.text]}
            onPress={() => navigation.goBack()}>
            {t('cancel')}
          </Text>
        </CategoryForm>
      </View>
    );
  }
}
export default withTranslation()(connect(null)(ForecastNewCategoryBudgetView));
