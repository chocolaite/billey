import React from 'react';
import {Text, View} from 'react-native';
import {mainStyles} from '../../../assets';
import {withTranslation} from 'react-i18next';
import {connect} from 'react-redux';
import {deleteCategory, setCategoryBudget} from '../actions';
import {CategoryForm} from '../components/Form';

class EditCategoryBudgetView extends React.Component {
  handleOnSave = (category) => {
    const {dispatch, navigation} = this.props;
    dispatch(setCategoryBudget(category)).then(() => {
      navigation.goBack();
    });
  };

  handleOnDelete = (category) => {
    const {dispatch, navigation} = this.props;
    dispatch(deleteCategory(category)).then((_) => {
      navigation.goBack();
    });
  };
  render() {
    const {route, t, currency} = this.props;
    const {category} = route.params;
    return (
      <View style={mainStyles.container}>
        <CategoryForm
          name={category.title}
          amount={category.text}
          keyText={category.keyText}
          currency={currency}
          onSave={(c) => this.handleOnSave(c)}>
          <Text
            style={[mainStyles.textAlignCenter, mainStyles.text]}
            onPress={() => this.handleOnDelete(category)}>
            {t('delete')}
          </Text>
        </CategoryForm>
      </View>
    );
  }
}
const mapStateToProps = (state) => {
  const {firstBudgetReducer} = state.budgetReducer;
  return {
    currency: firstBudgetReducer.currency,
  };
};
export default withTranslation()(
  connect(mapStateToProps)(EditCategoryBudgetView),
);
