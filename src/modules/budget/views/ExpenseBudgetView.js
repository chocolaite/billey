import React from 'react';
import {Platform, ScrollView, Text} from 'react-native';
import {withTranslation} from 'react-i18next';
import {mainStyles} from '../../../assets';
import {ExpenseForm} from '../components/Form';
import {connect} from 'react-redux';
import {
  fillExpensePickerSelect,
  getAverageExpense,
  saveExpense,
  updateDataWithExpense,
} from '../actions';
import KeyboardSpacer from 'react-native-keyboard-spacer';

class ExpenseBudgetView extends React.Component {
  componentDidMount() {
    const {dispatch} = this.props;
    dispatch(fillExpensePickerSelect());
  }

  handleOnCancel = () => {
    const {navigation} = this.props;
    navigation.goBack();
  };

  handleOnSave = (amount, category) => {
    const {dispatch, navigation} = this.props;
    dispatch(saveExpense(amount, category)).then((expense) => {
      dispatch(updateDataWithExpense(expense));
      dispatch(getAverageExpense());
      navigation.goBack();
    });
  };

  render() {
    const {t, categories, currency} = this.props;
    return (
      <ScrollView contentContainerStyle={mainStyles.container}>
        <Text style={mainStyles.h1}>{t('payment')}</Text>
        <ExpenseForm
          currency={currency}
          onSubmit={this.handleOnSave}
          dataPickerSelect={categories}
          onCancel={this.handleOnCancel}
        />
        {Platform.OS === 'ios' ? <KeyboardSpacer /> : null}
      </ScrollView>
    );
  }
}
const mapStateToProps = (state) => {
  const {inputReducer, appReducer} = state.budgetReducer;
  return {
    categories: inputReducer.dataPickerSelect,
    currency: appReducer.currency,
  };
};
export default withTranslation()(connect(mapStateToProps)(ExpenseBudgetView));
