import React from 'react';
import {Text, View} from 'react-native';
import {withTranslation} from 'react-i18next';
import styles from './styles';
import {mainStyles} from '../../../assets';
import {removeOldBudget, saveLocalBudget} from '../actions';
import {connect} from 'react-redux';
import {SAVE_BUDGET} from '../actions/types';
import {CommonActions} from '@react-navigation/native';
import Button from '../../../components/Button/Button';

class SaveForecastBudgetView extends React.Component {
  handleOnSave() {
    removeOldBudget().then(() => {
      const {dispatch, navigation} = this.props;
      dispatch(saveLocalBudget(SAVE_BUDGET)).then(() => {
        const resetAction = CommonActions.reset({
          index: 1,
          routes: [{name: 'Index'}],
        });
        navigation.dispatch(resetAction);
      });
    });
  }
  render() {
    const {t, navigation} = this.props;
    return (
      <View style={mainStyles.container}>
        <View style={mainStyles.f1}>
          <Text style={mainStyles.h1}>{t('new_forecast')}</Text>
        </View>
        <View style={styles.containerCheckBoxSaveForecast}>
          <Text style={[mainStyles.text, styles.txtCheckBox]} lineBreakMode={2}>
            {t('if_you_add_new_budget_the_old_budget_delete')}
          </Text>
        </View>
        <View style={styles.containerBtnSaveForecast}>
          <Button
            title={t('save')}
            onPress={() => this.handleOnSave()}
            style={mainStyles.mb_2}
          />
          <Text
            style={[mainStyles.textAlignCenter, mainStyles.text]}
            onPress={() => navigation.navigate('Budget')}>
            {t('cancel')}
          </Text>
        </View>
      </View>
    );
  }
}
export default withTranslation()(connect(null)(SaveForecastBudgetView));
