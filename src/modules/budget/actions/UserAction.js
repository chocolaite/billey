import AccountRepository from '../repositories/AccountRepository';
import AccountModel from '../models/AccountModel';

export async function getAccounts(): AccountModel[] {
  const repoAccount = new AccountRepository();
  return repoAccount.getAccounts();
}
