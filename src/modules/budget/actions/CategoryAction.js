import {CATEGORIES_BUDGET, FIRST_CATEGORIES_BUDGET} from './types';
import CategoryModel from '../models/CategoryModel';
import CategoryRepository from '../repositories/CategoryRepository';

export function deleteCategory(category, type = FIRST_CATEGORIES_BUDGET) {
  return (dispatch, getState) =>
    new Promise((resolve) => {
      const {budgetReducer} = getState();
      let categories = [];
      if (type === FIRST_CATEGORIES_BUDGET) {
        categories = budgetReducer.firstBudgetReducer;
      } else {
        categories = budgetReducer.infoBudgetReducer.categories;
      }
      const copyCategories = JSON.parse(JSON.stringify(categories));
      const newCategories = copyCategories.filter(
        (c) => c.keyText !== category.keyText,
      );
      dispatch(updateCategory(type, newCategories));
      resolve();
    });
}
export function getCategories(): CategoryModel[] {
  return async (dispatch) => {
    const repoCategory = new CategoryRepository();
    const categories = await repoCategory.getCategories();
    const categoriesForView = categories.map((item) => item.toObjView());
    dispatch(categoriesBudget(categoriesForView));
    return categoriesForView;
  };
}
export const categoriesBudget = (data) => ({
  type: CATEGORIES_BUDGET,
  value: data,
});
export const updateCategory = (type, data) => ({
  type,
  value: data,
});
