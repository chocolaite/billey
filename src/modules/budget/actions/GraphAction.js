import i18n from '../../../i18n';
import {DATA_BAR_GRAPH, TOTAL_BUDGET} from './types';
import CategoryRepository from '../repositories/CategoryRepository';
import ExpenseRepository from '../repositories/ExpenseRepository';
import BudgetCategoryRepository from '../repositories/BudgetCategoryRepository';

export function getDataBarGraph() {
  return (dispatch) =>
    new Promise(async (resolve) => {
      const repoBuCa = new BudgetCategoryRepository();
      const budgetAmount = await repoBuCa.getBudgetCategory();
      const repoCategory = new CategoryRepository();
      const repoExpense = new ExpenseRepository();
      const xyAxis = [];
      let total = 0;
      for (const item of budgetAmount) {
        const category = await repoCategory.getCategory({id: item.category_id});
        let amountTotal = item.amount;
        if (category !== undefined) {
          const obj = xyAxis.find((data) => data.id === category.id);
          if (obj === undefined) {
            const xStr =
              i18n.t(category.key_text).toString().substring(0, 4) + '...';
            const amountExpenses = await repoExpense.getAmountExpenses(
              item.category_id,
              item.budget_id,
            );
            amountTotal -= amountExpenses;
            xyAxis.push({
              x: xStr,
              y: amountTotal,
              color: category.color,
              category_id: category.id,
            });
          }
        }
        total += amountTotal;
      }
      dispatch(totalBudget(total));
      dispatch(dataBarGraph(xyAxis));
      resolve(xyAxis);
    });
}

export function getColor(i) {
  const colors = [
    '#003f5c',
    '#2f4b7c',
    '#665191',
    '#a05195',
    '#d45087',
    '#f95d6a',
    '#ff7c43',
    '#ffa600',
  ];
  if (i >= colors.length || i < 0) {
    return colors[Math.floor(Math.random() * colors.length)];
  }
  return colors[i];
}

export const totalBudget = (total) => ({
  type: TOTAL_BUDGET,
  value: total,
});

export const dataBarGraph = (data) => ({
  type: DATA_BAR_GRAPH,
  value: data,
});
