import CategoryRepository from '../repositories/CategoryRepository';
import {DATA_PICKER_SELECT} from './types';
import i18n from '../../../../src/i18n';

export function fillExpensePickerSelect() {
  return async (dispatch) => {
    const repo = new CategoryRepository();
    const categories = await repo.getCategories();
    const data = categories.map((item) => ({
      label: i18n.t(item.key_text),
      value: item.key_text,
    }));
    dispatch(expensePickerSelect(data));
  };
}

export const expensePickerSelect = (value) => ({
  type: DATA_PICKER_SELECT,
  value,
});
