import {CURRENCY_BUDGET, NAME_ACCOUNT, TITLE_HOME} from './types';
import BudgetRepository from '../repositories/BudgetRepository';
import moment from 'moment';
import {globals} from '../../../assets';
import i18n from '../../../i18n';

export function loadAppData(accounts) {
  return (dispatch) =>
    new Promise(async (resolve) => {
      const repoBudget = new BudgetRepository();
      const {currency, name} = accounts[0];
      const budget = await repoBudget.getBudget({name});
      const startDate = moment(budget.start_date, globals.FORMAT_DATE);
      const startDateMonthStr = startDate.format('MMM');
      const startDateYearStr = startDate.year();

      const endDate = moment(budget.end_date, globals.FORMAT_DATE);
      const endDateMonthStr = endDate.format('MMM');
      const endDateYearStr = endDate.year();

      let titleForHome =
        i18n.t(startDateMonthStr, `${startDateMonthStr}.`) +
        ' - ' +
        i18n.t(endDateMonthStr, `${endDateMonthStr}.`);
      if (startDateYearStr === endDateYearStr) {
        titleForHome += ` ${startDateYearStr}`;
      } else {
        titleForHome += ` ${startDateYearStr}/${endDateYearStr}`;
      }
      dispatch(titleHome(titleForHome));
      dispatch(currencyBudget(currency));
      dispatch(nameAccount(name));
      resolve();
    });
}
export const currencyBudget = (currency) => ({
  type: CURRENCY_BUDGET,
  value: currency,
});
export const nameAccount = (value) => ({
  type: NAME_ACCOUNT,
  value,
});
export const titleHome = (value) => ({
  type: TITLE_HOME,
  value,
});
