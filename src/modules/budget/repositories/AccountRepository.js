import AccountModel from '../models/AccountModel';
import {Repository} from './Repository';

export default class AccountRepository extends Repository {
  insert(args: {}): Promise {
    this._sql =
      'INSERT INTO Accounts (name, currency, created_at) VALUES(?,?,?)';
    return super.insert(args);
  }

  async getAccounts(): AccountModel[] {
    this._sql = 'SELECT * FROM Accounts';
    return this.getItems();
  }

  _createInstance(args): Object {
    const model = new AccountModel(args.name, args.currency);
    if (args.hasOwnProperty('id')) {
      model.id = args.id;
    }
    this._model = model;
  }
}
