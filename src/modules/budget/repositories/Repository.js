import {db} from '../../../config';
import i18n from '../../../i18n';

export class Repository {
  _sql = '';
  _data = [];
  _model = {};

  constructor() {
    if (this.constructor === Repository) {
      throw new TypeError(
        'Abstract class "Repository" cannot be instantiated directly',
      );
    }
  }

  insert(args: {}): Promise {
    this._createInstance(args);
    const sql = this._sql;
    const data = this._model.getAttrInArray();
    return new Promise(async (resolve, reject) => {
      await db
        .executeSql(sql, data)
        .then((results) => {
          const isInsert = results.rowsAffected > 0;
          if (isInsert) {
            this._model.id = results.insertId;
            resolve(this._model);
          } else {
            reject(new Error(i18n.t('error_occurred_please_try_again')));
          }
        })
        .catch(() => {
          reject(new Error(i18n.t('error_occurred_please_try_again')));
        })
        .finally(() => {
          this._sql = '';
          this._data = [];
          this._model = {};
        });
    });
  }

  async getItems(): [] {
    const sql = this._sql;
    const items = [];
    await db.executeSql(sql, []).then((results) => {
      const size = results.rows.length;
      for (let i = 0; i < size; i++) {
        const item = results.rows.item(i);
        this._createInstance(item);
        items.push(this._model);
      }
    });
    return items;
  }

  _createInstance(args) {
    this._model = {};
  }

  getModelInstance(args) {
    this._createInstance(args);
    return this._model;
  }

  removeAllData(): void {
    let sql = 'DELETE FROM Accounts';
    db.executeSql(sql).then((results) => {
      console.log('results accounts: ', results);
    });

    sql = 'DELETE FROM Budgets';
    db.executeSql(sql).then((results) => {
      console.log('results budgets: ', results);
    });

    sql = 'DELETE FROM Categories';
    db.executeSql(sql).then((results) => {
      console.log('results categories: ', results);
    });

    sql = 'DELETE FROM budgets_categories';
    db.executeSql(sql).then((results) => {
      console.log('results budgets_categories: ', results);
    });

    sql = 'DELETE FROM Expenses';
    db.executeSql(sql).then((results) => {
      console.log('results expenses: ', results);
    });
  }
}
