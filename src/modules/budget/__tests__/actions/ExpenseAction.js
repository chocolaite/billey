import thunk from 'redux-thunk';
import configureMockStore from 'redux-mock-store';
import {DATA_BAR_GRAPH, EXPENSE, TOTAL_BUDGET} from '../../actions/types';
import * as ExpenseAction from '../../actions/ExpenseAction';

const middlewares = [thunk];
let mockStore = configureMockStore(middlewares);
let store;

jest.mock('react-native', () => {
  const RN = jest.requireActual('react-native');
  RN.NativeModules.SettingsManager = {
    settings: {
      AppleLocale: 'en_US',
    },
  };
  return RN;
});

describe('ExpenseAction', () => {
  beforeEach(() => {
    // eslint-disable-next-line no-unused-vars
    store = mockStore();
  });
  it('should create an action to update data of the graph bar', () => {
    const data = 'Test';
    const expected = {
      type: DATA_BAR_GRAPH,
      value: data,
    };
    expect(ExpenseAction.updateDataBarGraph(data)).toEqual(expected);
  });
  it('should create an action to update data of the expense', () => {
    const data = 'Test';
    const expected = {
      type: EXPENSE,
      value: data,
    };
    expect(ExpenseAction.updateExpense(data)).toEqual(expected);
  });
  it('should create an action to update data of the total budget', () => {
    const data = 'Test';
    const expected = {
      type: TOTAL_BUDGET,
      value: data,
    };
    expect(ExpenseAction.updateTotal(data)).toEqual(expected);
  });
});
