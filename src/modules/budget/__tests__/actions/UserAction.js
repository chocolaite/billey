import AccountRepository from '../../repositories/AccountRepository';
import * as UserAction from '../../actions/UserAction';

jest.mock('react-native', () => {
  const RN = jest.requireActual('react-native');
  RN.NativeModules.SettingsManager = {
    settings: {
      AppleLocale: 'en_US',
    },
  };
  return RN;
});

describe('UserAction', () => {
  it('should has account', async () => {
    const mockGetAccount = jest.fn();
    jest.doMock('../../repositories/AccountRepository');
    AccountRepository.prototype.getAccounts = mockGetAccount;
    mockGetAccount.mockReturnValue([{currency: 'chf', name: 'name'}]);
    await UserAction.getAccounts().then((accounts) => {
      expect(accounts.length).toEqual(1);
    });
  });
});
