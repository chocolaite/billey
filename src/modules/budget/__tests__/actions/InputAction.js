import thunk from 'redux-thunk';
import configureMockStore from 'redux-mock-store';
import * as InputAction from '../../actions/InputAction';
import {DATA_PICKER_SELECT} from '../../actions/types';
import CategoryRepository from '../../repositories/CategoryRepository';
import CategoryModel from '../../models/CategoryModel';
import i18n from '../../../../i18n';

jest.mock('react-native', () => {
  const RN = jest.requireActual('react-native');
  RN.NativeModules.SettingsManager = {
    settings: {
      AppleLocale: 'en_US',
    },
  };
  return RN;
});

describe('InputAction', () => {
  const middlewares = [thunk];
  let mockStore = configureMockStore(middlewares);
  let store;
  beforeEach(() => {
    store = mockStore();
  });
  it('should create an action to update the picker select', () => {
    const value = 'Test';
    const expected = {
      type: DATA_PICKER_SELECT,
      value,
    };
    expect(InputAction.expensePickerSelect(value)).toEqual(expected);
  });
  it('should fill the picker select with the categories', async () => {
    const categories = getCategories();

    const mockGetCategories = jest.fn();
    jest.doMock('../../repositories/CategoryRepository');
    CategoryRepository.prototype.getCategories = mockGetCategories;
    mockGetCategories.mockReturnValue(categories);

    const values = categories.map((item) => ({
      label: i18n.t(item.key_text),
      value: item.key_text,
    }));

    const expected = [InputAction.expensePickerSelect(values)];
    await store.dispatch(InputAction.fillExpensePickerSelect());

    expect(store.getActions()).toEqual(expected);
  });
  function getCategories(): CategoryModel[] {
    return [
      new CategoryModel('test', 'white'),
      new CategoryModel('test2', 'white'),
    ];
  }
});
