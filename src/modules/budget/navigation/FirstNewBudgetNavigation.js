import React from 'react';
import {createStackNavigator, TransitionPresets} from '@react-navigation/stack';
import {Icon} from 'react-native-elements';
import NameBudgetView from '../views/NameBudgetView';
import TimelineBudgetView from '../views/TimelineBudgetView';
import CurrencyBudgetView from '../views/CurrencyBudgetView';
import CategoryBudgetView from '../views/CategoryBudgetView';
import EditCategoryBudgetView from '../views/EditCategoryBudgetView';
import NewCategoryBudgetView from '../views/NewCategoryBudgetView';
import {mainStyles} from '../../../assets';

const Stack = createStackNavigator();
const headerWithoutBarTitle = {
  headerStyle: {
    borderBottomWidth: 0,
    elevation: 0,
    shadowOffset: {
      width: 0,
      height: 0,
    },
  },
  title: '',
};
export const FirstNewBudgetNavigation = () => (
  <Stack.Navigator
    initialRouteName="Name_budget"
    screenOptions={{
      ...headerWithoutBarTitle,
    }}>
    <Stack.Screen name="Name_budget" component={NameBudgetView} />
    <Stack.Screen name="Timeline" component={TimelineBudgetView} />
    <Stack.Screen name="Currency" component={CurrencyBudgetView} />
    <Stack.Screen
      name="Category"
      component={CategoryBudgetView}
      options={({navigation}) => ({
        headerRight: () => (
          <Icon
            type={'font-awesome-5'}
            containerStyle={mainStyles.btnMenuRight}
            size={35}
            name="plus"
            onPress={() => {
              navigation.navigate('New_category');
            }}
          />
        ),
      })}
    />
    <Stack.Screen
      name={'Edit_category'}
      component={EditCategoryBudgetView}
      options={{
        ...TransitionPresets.ModalTransition,
      }}
    />
    <Stack.Screen
      name={'New_category'}
      component={NewCategoryBudgetView}
      options={{
        ...TransitionPresets.ModalTransition,
      }}
    />
  </Stack.Navigator>
);
