import {DATA_BAR_GRAPH, EXPENSE, TOTAL_BUDGET} from '../actions/types';

const initState = {
  total: 0,
  dataBarGraph: [],
  expense: 0,
};

export function homeBudgetReducer(state = initState, action) {
  let nextState;
  const {type, value} = action;
  if (type === TOTAL_BUDGET) {
    nextState = {
      ...state,
      total: value,
    };
    return nextState;
  }
  if (type === DATA_BAR_GRAPH) {
    nextState = {
      ...state,
      dataBarGraph: value,
    };
    return nextState;
  }
  if (type === EXPENSE) {
    nextState = {
      ...state,
      expense: value,
    };
    return nextState;
  }
  return state;
}
