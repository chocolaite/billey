import {CURRENCY_BUDGET, NAME_ACCOUNT, TITLE_HOME} from '../actions/types';

const initState = {
  currency: '',
  name: '',
  titleHome: '',
};

export function appReducer(state = initState, action) {
  let nextState;
  const {type, value} = action;
  if (type === NAME_ACCOUNT) {
    nextState = {
      ...state,
      name: value,
    };
    return nextState;
  }
  if (type === CURRENCY_BUDGET) {
    nextState = {
      ...state,
      currency: value,
    };
    return nextState;
  }
  if (type === TITLE_HOME) {
    nextState = {
      ...state,
      titleHome: value,
    };
    return nextState;
  }
  return state;
}
