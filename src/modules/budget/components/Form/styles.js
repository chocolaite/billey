import {StyleSheet, Platform} from 'react-native';
import {mainStyles} from '../../../../assets';

export default StyleSheet.create({
  containerExpenseForm: {
    flex: 1,
  },
  containerInputExpenseForm: {
    flex: 1,
    marginTop: 20,
  },
  txtCategoryLbl: {
    marginLeft: 10,
    fontSize: 15,
  },
  inputSelectContainer: {
    ...mainStyles.input,
    paddingTop: Platform.OS === 'ios' ? 10 : 0,
    marginLeft: 10,
    marginRight: 10,
  },
  containerAmount: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 30,
  },
  txtAmount: {
    textAlign: 'center',
    fontSize: 50,
  },
  containerBtnExpenseForm: {
    flex: 1,
  },
  txtCancelExpenseForm: {
    marginTop: 20,
    textAlign: 'center',
  },
  inputError: {
    ...mainStyles.inputError,
    paddingTop: 10,
    marginLeft: 10,
    marginRight: 10,
  },
  txtErrorPicker: {
    marginLeft: 15,
    fontSize: 12,
  },
  inputAndroid: {
    paddingVertical: 8,
  },
  containerAmountEditCatForm: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: Platform.OS === 'ios' ? 0 : 20,
  },
  amountEditCatForm: {
    textAlign: 'center',
    fontSize: 50,
  },
  containerBtnEditCatForm: {
    flex: 1,
    marginBottom: 20,
  },
});
