import {Text, TouchableWithoutFeedback, View} from 'react-native';
import moment from 'moment';
import React, {useState} from 'react';
import {globals, mainStyles} from '../../../../assets';
import styles from './styles';
import DateTimePickerModal from 'react-native-modal-datetime-picker';

export const DateInput = ({label, date, onChange, error = ''}) => {
  const [show, isShow] = useState(false);
  const mDateObj = moment(date, globals.FORMAT_DATETIME).toDate();

  const handleOnChange = (dte: Date) => {
    isShow(false);
    if (dte !== undefined) {
      const mDate = moment(dte);
      onChange(mDate.format(globals.FORMAT_DATE));
    }
  };
  return (
    <View style={mainStyles.w100}>
      <Text style={mainStyles.inputLabel}>{label}</Text>
      <TouchableWithoutFeedback onPress={() => isShow(true)}>
        <View style={[mainStyles.input, styles.dateInputComp]}>
          <Text>{date}</Text>
        </View>
      </TouchableWithoutFeedback>
      {show && (
        <DateTimePickerModal
          date={mDateObj}
          isVisible={show}
          mode="date"
          onConfirm={handleOnChange}
          onCancel={() => isShow(false)}
        />
      )}
      {error.length <= 0 ? null : (
        <Text style={mainStyles.txtError}>{error}</Text>
      )}
    </View>
  );
};
