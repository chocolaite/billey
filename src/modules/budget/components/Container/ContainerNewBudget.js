import React from 'react';
import {Platform, ScrollView, Text, View} from 'react-native';
import styles from './styles';
import {mainStyles} from '../../../../assets';
import DesignByGT from '../../../../components/Brand/DesignByGT';
import i18n from '../../../../i18n';
import KeyboardSpacer from 'react-native-keyboard-spacer';

export default ({text = '', children}) => (
  <ScrollView contentContainerStyle={styles.containerNewBud}>
    <View style={styles.contentNewBud}>
      <Text style={mainStyles.h1}>{i18n.t('app_name')}</Text>
      <Text style={[mainStyles.text, styles.textNewBud]}>{text}</Text>
      {children}
      <DesignByGT style={mainStyles.pt_3} />
    </View>
    {Platform.OS === 'ios' ? <KeyboardSpacer /> : null}
  </ScrollView>
);
