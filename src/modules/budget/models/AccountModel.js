import moment from 'moment';
import {globals} from '../../../assets';

export default class AccountModel {
  /** @var {datetime} created_at*/
  #created_at;
  /** @var {number} id*/
  #_id = -1;

  /**
   * @param {string} name
   * @param {string} currency
   * */
  constructor(name: string, currency: string) {
    this.name = name;
    this.currency = currency;
    this.#created_at = moment().format(globals.FORMAT_DATETIME);
  }

  getAttrInArray() {
    return [this.name, this.currency, this.#created_at];
  }

  get id(): number {
    return this.#_id;
  }

  set id(value: number) {
    this.#_id = value;
  }
}
