export default class BudgetCategoryModel {
  constructor(amount: number, category_id: number, budget_id: number) {
    this.amount = amount;
    this.category_id = category_id;
    this.budget_id = budget_id;
  }
  getAttrInArray() {
    return [this.amount, this.category_id, this.budget_id];
  }
}
