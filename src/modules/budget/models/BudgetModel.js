import moment from 'moment';
import {globals} from '../../../assets';
import CategoryModel from './CategoryModel';

export default class BudgetModel {
  /** @var {datetime} created_at*/
  #created_at;
  /** @var {number} id*/
  #_id = -1;
  /** @var {number} _account_id*/
  #_account_id;
  /** @var {CategoryModel[]} _categories*/
  #_categories = [];
  /**
   * @param {Date} start_date
   * @param {Date} end_date
   * */
  constructor(start_date: string, end_date: string) {
    this.start_date = start_date;
    this.end_date = end_date;
    this.#created_at = moment().format(globals.FORMAT_DATETIME);
  }

  getAttrInArray() {
    return [
      this.#_account_id,
      this.start_date,
      this.end_date,
      this.#created_at,
    ];
  }

  get created_at() {
    return this.#created_at;
  }

  get id(): number {
    return this.#_id;
  }

  set id(value: number) {
    this.#_id = value;
  }

  get account_id(): number {
    return this.#_account_id;
  }

  set account_id(value: number) {
    this.#_account_id = value;
  }

  get categories(): CategoryModel[] {
    return this.#_categories;
  }

  addCategory(item: CategoryModel) {
    this.#_categories.push(item);
  }
}
