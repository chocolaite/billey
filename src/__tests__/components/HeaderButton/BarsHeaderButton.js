import renderer from 'react-test-renderer';
import BarsHeaderButton from '../../../components/HeaderButton/BarsHeaderButton';
import React from 'react';

describe('BarsHeaderButton', () => {
  it('should render correctly component', () => {
    const tree = renderer
      .create(<BarsHeaderButton onPress={() => {}} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});
